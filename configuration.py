# ELASTIC_IP = '192.168.43.34'
ELASTIC_IP = '192.168.7.173'
ELASTIC_PORT = '9200'

RETRY_TIMEOUT = 0.3

RAW_KEY_MAP = {
    'H0': 'harmonic_1',
    'H1': 'harmonic_2',
    'H2': 'harmonic_3',
    'H3': 'harmonic_4',
    'H4': 'harmonic_5',
    'H5': 'harmonic_6',
    'ID': 'id',
    'LL': 'light_level',
    'NT': 'nr_transient',
    'PC': 'power_current',
    'PTA': 'power_transient_average',
    'PTP': 'power_transient_peak',
    'TMP': 'temperature',
}