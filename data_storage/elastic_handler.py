def store_record(elastic_object, index_name, type_name, record):
    '''
    stores the data in the provided data_storage index
    '''
    try:
        elastic_object.index(index=index_name,
                             doc_type=type_name,
                             body=record)
    except Exception as ex:
        print('Error in indexing data')