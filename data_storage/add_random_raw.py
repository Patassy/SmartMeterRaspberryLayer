from configuration import ELASTIC_IP, ELASTIC_PORT
from elasticsearch import Elasticsearch
from data_storage.elastic_handler import store_record


EXAMPLE_DATA = {
    "id": 1,
    "timestamp": "2018-01-01 15:00:00",
    "nr_transient": 1,
    "power_transient_average": 10,
    "power_transient_peak": 100,
    "power_current": 100,
    "harmonic_1": 1,
    "harmonic_2": 2,
    "harmonic_3": 3,
    "harmonic_4": 4,
    "harmonic_5": 5,
    "harmonic_6": 6,
    "temperature": 7,
    "light_level": 8
}

if __name__ == '__main__':
    ES = Elasticsearch([{'host': ELASTIC_IP, 'port': int(ELASTIC_PORT)}])
    store_record(elastic_object=ES,
                 index_name='smartmeter',
                 type_name='raw_data',
                 record=EXAMPLE_DATA)
