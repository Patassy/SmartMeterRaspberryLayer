'''
This script creates a type called raw_data in the index smartmeter

author: patex1987
'''

from elasticsearch import Elasticsearch
from configuration import ELASTIC_IP, ELASTIC_PORT


def create_index(es_object, index_name='smartmeter'):
    created = False
    # index settings
    settings = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0
        },
        "mappings": {
            "raw_data": {
                "dynamic": "strict",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "timestamp": {
                        "type":   "date",
                        "format": "yyyy-MM-dd HH:mm:ss"
                    },
                    "nr_transient": {
                        "type": "integer"
                    },
                    "power_transient_average": {
                        "type": "integer"
                    },
                    "power_transient_peak": {
                        "type": "integer"
                    },
                    "power_current": {
                        "type": "integer"
                    },
                    "harmonic_1": {
                        "type": "integer"
                    },
                    "harmonic_2": {
                        "type": "integer"
                    },
                    "harmonic_3": {
                        "type": "integer"
                    },
                    "harmonic_4": {
                        "type": "integer"
                    },
                    "harmonic_5": {
                        "type": "integer"
                    },
                    "harmonic_6": {
                        "type": "integer"
                    },
                    "temperature": {
                        "type": "float"
                    },
                    "light_level": {
                        "type": "float"
                    },
                }
            }
        }
    }


    try:
        if not es_object.indices.exists(index_name):
            # Ignore 400 means to ignore "Index Already Exist" error.
            es_object.indices.create(index=index_name, ignore=400, body=settings)
            print('Created Index')
            created = True
    except Exception as ex:
        print(str(ex))
    finally:
        return created


if __name__ == '__main__':
    ES = Elasticsearch([{'host': ELASTIC_IP, 'port': int(ELASTIC_PORT)}])
    create_index(ES)
