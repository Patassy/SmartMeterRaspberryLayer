'''
This script is responsible for collecting data from a smart meter,  and sending to elasticsearch
'''
import time
from datetime import datetime
from configuration import ELASTIC_PORT, ELASTIC_IP, RETRY_TIMEOUT, RAW_KEY_MAP
from elasticsearch import Elasticsearch
from smartmeter.smart_meter import SmartMeter
from exceptions.custom_exceptions import NoSerialData, CorruptedData, ElasticConnectionError
from data_storage.elastic_handler import store_record
import serial
from urllib3.exceptions import ConnectTimeoutError, NewConnectionError
from helpers.log_helper import get_logger

EXPECTED_EXCEPTIONS = (NoSerialData,
                       CorruptedData,
                       serial.SerialException,
                       ElasticConnectionError,
                       ConnectTimeoutError,
                       NewConnectionError,
                       OSError)


class ElasticMiddleware(object):
    '''
    Middle layer between Smart meter and Elastic
    '''
    def __init__(self,
                 port,
                 interval=1.0):
        self._smart_meter = SmartMeter(port=port)
        self._interval = interval
        self._collection_state = False
        self._es = Elasticsearch([{'host': ELASTIC_IP, 'port': int(ELASTIC_PORT)}],
                                 retry_on_timeout=True)
        self._logger = get_logger()
        self._logger.info('Initialized')

    def start_collection(self):
        '''
        Starts cyclic collection of sensor data

        :return:
        '''
        self._collection_state = True
        while self._collection_state:
            try:
                self._handle_measured_data()
            except EXPECTED_EXCEPTIONS as ex:
                self._logger.info(msg=str(ex))
                time.sleep(RETRY_TIMEOUT)
                continue
            time.sleep(self._interval)

    def stop_collection(self):
        '''
        Stops the data collection
        '''
        self._collection_state = False

    def _handle_measured_data(self):
        '''
        Converts raw data, Sends it to data_storage

        :return:
        '''
        raw_measurement = self._smart_meter.get_measured_data()
        if raw_measurement == '':
            raise NoSerialData('Missing data from sensor')
        structured_measurement = self._parse_sensor_data(raw_measurement)
        self._logger.info(msg=structured_measurement)
        store_record(elastic_object=self._es,
                     index_name='smartmeter',
                     type_name='raw_data',
                     record=structured_measurement)


    def _parse_sensor_data(self, raw_measurement):
        '''
        Converts the raw sensor output into a dictionary

        :return: dict
        '''
        try:
            arr = raw_measurement.split()
            raw = dict(zip(arr[::2], arr[1::2]))
            parsed_data = {RAW_KEY_MAP[name]: val for name, val in raw.items()}
        except (KeyError, IndexError, AttributeError):
            raise CorruptedData('Data is corrupted')
        act_timestamp = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        parsed_data['timestamp'] = act_timestamp
        return parsed_data
