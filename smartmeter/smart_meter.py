'''
Class for Smart meter communication
'''

import serial


class SmartMeter(object):
    '''
    Class for handling communication with the Smart Meter
    '''

    def __init__(self,
                 port,
                 baudrate=9600,
                 bytesize=serial.EIGHTBITS,
                 parity=serial.PARITY_NONE,
                 stopbits=serial.STOPBITS_ONE,
                 timeout=0.5):
        self._port = port
        self._baudrate = baudrate
        self._bytesize = bytesize
        self._parity = parity
        self._stopbits = stopbits
        self._timeout = timeout

    def get_measured_data(self):
        '''
        Connects to the COM port and retrieves the data

        :return: raw string from smart meter
        '''
        raw_data = None
        with self._get_serial_object() as meter:
            meter.write(b'p')
            raw_data = meter.readline()
            raw_data = raw_data.decode('utf-8')
        return raw_data

    def _get_serial_object(self):
        '''
        Creates a connection with the smart meter

        :return: pyserial object of the smart meter
        '''
        meter = serial.Serial()
        meter.baudrate = self._baudrate
        meter.port = self._port
        meter.bytesize = self._bytesize
        meter.parity = self._parity
        meter.stopbits = self._stopbits
        meter.timeout = self._timeout
        return meter
