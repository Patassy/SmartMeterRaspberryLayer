import atexit
from smartmeter.elastic_middleware import ElasticMiddleware
import time


def exit_handler():
    if HANDLER is not None:
        HANDLER.stop_collection()
        time.sleep(2)


if __name__ == '__main__':
    atexit.register(exit_handler)
    HANDLER = ElasticMiddleware('/dev/ttyUSB0')
    HANDLER.start_collection()
