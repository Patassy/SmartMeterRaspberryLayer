class NoSerialData(Exception):
    pass


class CorruptedData(Exception):
    pass


class ElasticConnectionError(Exception):
    pass